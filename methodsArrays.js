const equipos = [
	{ nombre: 'Nacional', ligas: 16, libertadores: 2, ciudad: 'Medellin' },
	{ nombre: 'Medellin', ligas: 6, libertadores: 0, ciudad: 'Medellin' },
	{ nombre: 'Millonarios', ligas: 15, libertadores: 0, ciudad: 'Bogota' },
	{ nombre: 'America', ligas: 14, libertadores: 0, ciudad: 'Cali' },
	{ nombre: 'Once Caldas', ligas: 4, libertadores: 1, ciudad: 'Manizales' }
];
/**
 * ES5
 */
let nombresE = [];
for (let i = 0; i < equipos.length; i++) {
	nombresE.push(equipos[i].nombre);
}
console.log(nombresE);
/**
 * ES6 MAP
 */
const nombresEquipos = equipos.map(equipo => equipo.nombre);
console.log(nombresEquipos);

/**
 * Find
 */
const buscar = equipos.find(equipo => equipo.ciudad === 'Medellin');
console.log(buscar);

/**
 * Every
 */

const winLibertadores = equipos.every(equipo => equipo.libertadores > 0);
console.log(winLibertadores);

const winLigas = equipos.every(equipo => equipo.ligas > 0);
console.log(winLigas);

/**
 * Some
 */

const someLibertadores = equipos.some(equipo => equipo.libertadores > 0);
console.log(someLibertadores);

/**
 * Filter
 */

const losMejores = equipos.filter(equipo => equipo.libertadores > 0).map(equipo => equipo.nombre);
console.log(losMejores);

/**
 * Reduce
 */

const cantidadLib = equipos.reduce((total, equipo) => equipo.libertadores + total, 0);
console.log(cantidadLib);

const cantidaL = equipos
	.filter(equipo => equipo.libertadores > 0)
	.reduce((total, equipo) => total + equipo.libertadores, 0);
console.log(cantidaL);
