/**
 * Var
 */

(function explicarVar() {
	var vvar = 30;
	if (true) {
		var vvar = 45;
	}
	console.log(vvar);
})();

/**
 * Let
 */
(function explicarLet() {
	let lvar = 30;
	if (true) {
		let lvar = 45;
	}
	console.log(lvar);
})();

/**
 * Const
 */

(function explicarLet() {
	const cvar = 30;
	if (true) {
		const cvar = 45;
	}
	console.log(cvar);
})();

const persona = {
	name: 'Juan',
	age: 35
};
/**
 * Const
 */
console.log(persona);
persona.name = 'Pedro';
console.log(persona);
