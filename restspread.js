/**
 * Operador rest ...
 */

function porPagar(info, ...valores) {
	let resultado = 0;
	// valores.forEach(function(valor){
	//     return resultado+=valor;
	// })
	valores.forEach(valor => (resultado += valor));
	return `${info} debemos: ${resultado}`;
}

/**
 * Spread
 * Enviar argumentos desde un arreglo
 */
const deudas = [100, 200, 500, 200];

/**
 * Spread
 * Asignar un elemento(s) a un arreglo
 */
const deudaInicial = 500;
const nuevaDeudad = [deudaInicial, ...deudas];
console.log(nuevaDeudad);
console.log(porPagar('Enero', ...nuevaDeudad));
