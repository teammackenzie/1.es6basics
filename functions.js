/**
 * Functions Declaration
 */
function cambiar(dato = 1000) {
	return dato * 2000;
}
const respuesta = cambiar();
console.log(respuesta);
/**
 * Functions Expressions
 */
const cambio = function(dato) {
	return dato * 2000;
};
const resultado = cambio(2000);
console.log(resultado);

const animal = {
	nombre: 'Firulais',
	edad: 10,
	hacer: function() {
		return 'ladrar';
	}
};
console.log(animal.hacer());
