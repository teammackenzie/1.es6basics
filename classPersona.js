class Persona {
	constructor(nombre, edad) {
		this.nombre = nombre;
		this.edad = edad;
	}
	saludar() {
		return `Hola ${this.nombre}`;
	}
}

const persona = new Persona('Gio', 25);
console.log(persona.nombre);
console.log(persona.saludar());
