/**
 * Destructuring Arrays
 */
const punto = [50, 30, -90, 40, 10];
const [x, y, ...otros] = punto;
console.log(x);
console.log(y);
console.log(otros);

/**
 * Destructuring Objects
 */
const { b, a } = { a: 10, b: 20 };
console.log(a);
console.log(b);

let abc = 50;
let def = 30;

[abc, def] = [def, abc];
console.log(abc);
console.log(def);
