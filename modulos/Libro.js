class Libro {
	constructor(name) {
		this.name = name;
	}
	showName() {
		return `Libro se llama ${this.name}`;
	}
}
export default Libro;
