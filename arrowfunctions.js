const tarea = function(persona, accion = 'Descansar') {
	return `${persona} va a realizar ${accion}`;
};
console.log(tarea('Juan', 'Jugar'));
const tarea2 = (persona, accion = 'Descansar') => {
	return `${persona} va a realizar ${accion}`;
};
console.log(tarea2('Juan', 'Jugar'));

const saludar = () => 'hola';
console.log(saludar());

